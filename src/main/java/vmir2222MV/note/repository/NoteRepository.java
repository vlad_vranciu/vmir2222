package vmir2222MV.note.repository;


import vmir2222MV.note.model.Nota;
import vmir2222MV.note.utils.ClasaException;

import java.util.List;

public interface NoteRepository {
	
	public void addNota(Nota nota) throws ClasaException;
	public List<Nota> getNote(); 
	public void readNote(String fisier);
	
}
