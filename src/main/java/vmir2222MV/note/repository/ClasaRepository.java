package vmir2222MV.note.repository;


import vmir2222MV.note.model.Corigent;
import vmir2222MV.note.model.Elev;
import vmir2222MV.note.model.Medie;
import vmir2222MV.note.model.Nota;
import vmir2222MV.note.utils.ClasaException;

import java.util.HashMap;
import java.util.List;

public interface ClasaRepository {
	
	public void creazaClasa(List<Elev> elevi, List<Nota> note);
	public HashMap<Elev, HashMap<String, List<Double>>> getClasa();
	public List<Medie> calculeazaMedii() throws ClasaException;
	public void afiseazaClasa();
	public List<Corigent> getCorigenti();
}
