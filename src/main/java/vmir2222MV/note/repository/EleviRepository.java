package vmir2222MV.note.repository;


import vmir2222MV.note.model.Elev;

import java.util.List;

public interface EleviRepository {
	public void addElev(Elev e);
	public List<Elev> getElevi();
	public void readElevi(String fisier);
}
