package vmir2222MV.note.main;


import vmir2222MV.note.controller.NoteController;
import vmir2222MV.note.model.Corigent;
import vmir2222MV.note.model.Medie;
import vmir2222MV.note.model.Nota;
import vmir2222MV.note.utils.ClasaException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

//functionalitati
//F01.	 adaugarea unei note la o anumita materie (nr. matricol, materie, nota acordata);
//F02.	 calcularea mediilor semestriale pentru fiecare elev (nume, nr. matricol),
//F03.	 afisarea elevilor coringenti, ordonati descrescator dupa numarul de materii la care nu au promovat şi alfabetic dupa nume.


public class StartApp {

	/**
	 * @param args
	 * @throws ClasaException
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		NoteController ctrl = new NoteController();
		List<Medie> medii = new LinkedList<Medie>();
		List<Corigent> corigenti = new ArrayList<Corigent>();
		ctrl.readElevi("D:\\Facultate\\VVSS\\Lab01\\Lab01\\src\\main\\java\\elevi.txt");
		ctrl.readNote("D:\\Facultate\\VVSS\\Lab01\\Lab01\\src\\main\\java\\note.txt");
		ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
		boolean gasit = false;
		while(!gasit) {
			System.out.println("1. Adaugare Nota");
			System.out.println("2. Calculeaza medii");
			System.out.println("3. Elevi corigenti");
			System.out.println("4. Iesire");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in) );
		    try {
				int option = Integer.parseInt(br.readLine());
				switch(option) {
				case 1:
					try {
						ctrl.addNota(getNota());
					} catch (ClasaException e) {
						System.out.println(e);
					}break;
					case 2:
						try {
							medii = ctrl.calculeazaMedii();
						} catch (ClasaException e) {
							System.out.println(e);
						}
						for(Medie medie:medii)
							System.out.println(medie);
						break;
				case 3: corigenti = ctrl.getCorigenti();
						for(Corigent corigent:corigenti)
							System.out.println(corigent);
						break;
				case 4: gasit = true;
						break;
				default: System.out.println("Introduceti o optiune valida!");
				}
				
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	private static Nota getNota(){
		Scanner scanner=new Scanner(System.in);
		double nrmatricol,nota;
		String materie;
		System.out.print("Introduceti numarul matricol al elevului: ");
		nrmatricol=scanner.nextDouble();
		System.out.print("Introduceti materia: ");
		scanner.nextLine();
		materie=scanner.nextLine();
		System.out.print("Introduceti nota: ");
		nota=scanner.nextDouble();
		return new Nota(nrmatricol,materie,nota);
	}

}
